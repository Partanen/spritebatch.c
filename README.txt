OpenGL sprite batching implementations/examples in C.
Included are versions for GLES 2.0 and OpenGL 2.0.

The GLES 2.0 version has been used on Android.

The GL 2.0 version has been tested on GNU/Linux and Windows. The correct
libraries will need to be installed in the correct include paths.
