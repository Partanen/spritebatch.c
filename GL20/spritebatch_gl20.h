#pragma once
#include <GL/glew.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

typedef enum SpriteFlip
{
     SPRITE_FLIP_NONE,
     SPRITE_FLIP_HORIZONTAL,
     SPRITE_FLIP_VERTICAL,
     SPRITE_FLIP_BOTH
} SpriteFlip;

typedef struct Texture              Texture;
typedef struct TextureBufferItem    TextureBufferItem;
typedef struct SpriteBatch          SpriteBatch;

/* Return 0 on success */
int
SpriteBatch_init(SpriteBatch *batch, GLchar *error_log, GLint log_len);

void
SpriteBatch_dispose(SpriteBatch *batch);

/* Note: setting clip to 0 is allowed to draw a full texture */
void
SpriteBatch_drawSprite(SpriteBatch *batch, Texture *texture,
                       int x, int y, unsigned int *clip);

void
SpriteBatch_drawSprite_Flip(SpriteBatch *batch, Texture *texture,
    int x, int y, unsigned int *clip, SpriteFlip flip);

void
SpriteBatch_drawSprite_Scale(SpriteBatch *batch, Texture *texture,
    int x, int y, unsigned int *clip, float scale_x, float scale_y);

void
SpriteBatch_drawSprite_Rotate(SpriteBatch *batch, Texture *texture,
    int x, int y, unsigned int *clip, float angle);

void
SpriteBatch_drawSprite_FlipScale(SpriteBatch *batch, Texture *texture,
    int x, int y, unsigned int *clip, SpriteFlip flip, float scale_x, float scale_y);

void
SpriteBatch_drawSprite_FlipRotate(SpriteBatch *batch, Texture *texture,
    int x, int y, unsigned int *clip, SpriteFlip flip, float angle);

void
SpriteBatch_drawSprite_ScaleRotate(SpriteBatch *batch, Texture *texture,
    int x, int y, unsigned int *clip, float scale_x, float scale_y, float angle);

void
SpriteBatch_drawSprite_FlipScaleRotate(SpriteBatch *batch, Texture *texture,
    int x, int y, unsigned int *clip,
    SpriteFlip flip, float scale_x, float scale_y, float angle);

void
SpriteBatch_flush(SpriteBatch *batch, int *viewport);

struct Texture
{
    GLuint          id;
    unsigned int    width;
    unsigned int    height;
};

struct TextureBufferItem
{
    GLuint id;
    unsigned int repeats;
};

struct SpriteBatch
{
    GLuint              program;
    GLuint              vbo;
    GLuint              ebo;
    GLint               proj_loc;
    GLint               scale_loc;

    GLfloat             *vertex_buffer;
    TextureBufferItem   *texture_buffer;
    unsigned int        buffer_size;

    unsigned int        sprite_count;
    unsigned int        tex_swap_count;

    GLfloat             scale_x;
    GLfloat             scale_y;
};

