#include "spritebatch_gles2.h"

#define BATCH_SIZE 1024
#define FLOATS_PER_SPRITE 16

/* Macro corner ordering:
 * 1. top left
 * 2. top right
 * 3. bottom left
 * 4. bottom right */

#define SPRITE_DRAW_FUNC_LOWER_HALF()\
    if (batch->sprite_count > 0)\
    {\
        if (batch->texture_buffer[batch->tex_swap_count].id == texture->id)\
            ++batch->texture_buffer[batch->tex_swap_count].repeats;\
        else\
        {\
            ++batch->tex_swap_count;\
            batch->texture_buffer[batch->tex_swap_count].id = texture->id;\
            batch->texture_buffer[batch->tex_swap_count].repeats = 0;\
        }\
    }\
    else\
    {\
        batch->texture_buffer[0].id = texture->id;\
        batch->texture_buffer[0].repeats = 0;\
    }\
    ++batch->sprite_count;

#define SET_CLIPPED_SPRITE_DEFAULT_POSITION()\
    batch->vertex_buffer[index +  0] = batch->scale_x * (GLfloat)x;\
    batch->vertex_buffer[index +  1] = batch->scale_y * (GLfloat)y;\
    batch->vertex_buffer[index +  4] = batch->scale_x * (GLfloat)(x + clip[2]);\
    batch->vertex_buffer[index +  5] = batch->scale_y * (GLfloat)y;\
    batch->vertex_buffer[index +  8] = batch->scale_x * (GLfloat)x;\
    batch->vertex_buffer[index +  9] = batch->scale_y * (GLfloat)(y + clip[3]);\
    batch->vertex_buffer[index + 12] = batch->scale_x * (GLfloat)(x + clip[2]);\
    batch->vertex_buffer[index + 13] = batch->scale_y * (GLfloat)(y + clip[3]);

#define SET_UNCLIPPED_SPRITE_DEFAULT_POSITION()\
    batch->vertex_buffer[index +  0] = batch->scale_x * (GLfloat)x;\
    batch->vertex_buffer[index +  1] = batch->scale_y * (GLfloat)y;\
    batch->vertex_buffer[index +  4] = batch->scale_x * (GLfloat)(x + texture->width);\
    batch->vertex_buffer[index +  5] = batch->scale_y * (GLfloat)y;\
    batch->vertex_buffer[index +  8] = batch->scale_x * (GLfloat)x;\
    batch->vertex_buffer[index +  9] = batch->scale_y * (GLfloat)(y + texture->height);\
    batch->vertex_buffer[index + 12] = batch->scale_x * (GLfloat)(x + texture->width);\
    batch->vertex_buffer[index + 13] = batch->scale_y * (GLfloat)(y + texture->height);

#define SET_UNCLIPPED_SPRITE_SCALED_POSITION()\
    batch->vertex_buffer[index +  0] = batch->scale_x * (GLfloat)x;\
    batch->vertex_buffer[index +  1] = batch->scale_y * (GLfloat)y;\
    batch->vertex_buffer[index +  4] = batch->scale_x * (GLfloat)(x + texture->width * scale_x);\
    batch->vertex_buffer[index +  5] = batch->scale_y * (GLfloat)y;\
    batch->vertex_buffer[index +  8] = batch->scale_x * (GLfloat)x;\
    batch->vertex_buffer[index +  9] = batch->scale_y * (GLfloat)(y + texture->height * scale_y);\
    batch->vertex_buffer[index + 12] = batch->scale_x * (GLfloat)(x + texture->width * scale_x);\
    batch->vertex_buffer[index + 13] = batch->scale_y * (GLfloat)(y + texture->height * scale_y);

#define SET_CLIPPED_SPRITE_SCALED_POSITION()\
    batch->vertex_buffer[index +  0] = batch->scale_x * (GLfloat)x;\
    batch->vertex_buffer[index +  1] = batch->scale_y * (GLfloat)y;\
    batch->vertex_buffer[index +  4] = batch->scale_x * (GLfloat)(x + clip[2] * scale_x);\
    batch->vertex_buffer[index +  5] = batch->scale_y * (GLfloat)y;\
    batch->vertex_buffer[index +  8] = batch->scale_x * (GLfloat)x;\
    batch->vertex_buffer[index +  9] = batch->scale_y * (GLfloat)(y + clip[3] * scale_y);\
    batch->vertex_buffer[index + 12] = batch->scale_x * (GLfloat)(x + clip[2] * scale_x);\
    batch->vertex_buffer[index + 13] = batch->scale_y * (GLfloat)(y + clip[3] * scale_y);

#define SET_CLIPPED_SPRITE_DEFAULT_CLIPS()\
    batch->vertex_buffer[index +  2] = (GLfloat)clip[0] / (GLfloat)texture->width;\
    batch->vertex_buffer[index +  3] = (GLfloat)clip[1] / (GLfloat)texture->height;\
    batch->vertex_buffer[index +  6] = (GLfloat)(clip[0] + clip[2]) / (GLfloat)texture->width;\
    batch->vertex_buffer[index +  7] = (GLfloat)clip[1] / (GLfloat)texture->height;\
    batch->vertex_buffer[index + 10] = (GLfloat)clip[0] / (GLfloat)texture->width;\
    batch->vertex_buffer[index + 11] = (GLfloat)(clip[1] + clip[3]) / (GLfloat)texture->height;\
    batch->vertex_buffer[index + 14] = (GLfloat)(clip[0] + clip[2]) / (GLfloat)texture->width;\
    batch->vertex_buffer[index + 15] = (GLfloat)(clip[1] + clip[3]) / (GLfloat)texture->height;\

#define SET_UNCLIPPED_SPRITE_DEFAULT_CLIPS()\
    batch->vertex_buffer[index +  2] = 0.0f;\
    batch->vertex_buffer[index +  3] = 0.0f;\
    batch->vertex_buffer[index +  6] = 1.0f;\
    batch->vertex_buffer[index +  7] = 0.0f;\
    batch->vertex_buffer[index + 10] = 0.0f;\
    batch->vertex_buffer[index + 11] = 1.0f;\
    batch->vertex_buffer[index + 14] = 1.0f;\
    batch->vertex_buffer[index + 15] = 1.0f;\

#define SET_CLIPPED_SPRITE_FLIPPED_CLIPS()\
    switch (flip)\
    {\
        case SPRITE_FLIP_NONE:\
        {\
            batch->vertex_buffer[index +  2] = (GLfloat)clip[0] / (GLfloat)texture->width;\
            batch->vertex_buffer[index +  3] = (GLfloat)clip[1] / (GLfloat)texture->height;\
            batch->vertex_buffer[index +  6] = (GLfloat)(clip[0] + clip[2]) / (GLfloat)texture->width;\
            batch->vertex_buffer[index +  7] = (GLfloat)clip[1] / (GLfloat)texture->height;\
            batch->vertex_buffer[index + 10] = (GLfloat)clip[0] / (GLfloat)texture->width;\
            batch->vertex_buffer[index + 11] = (GLfloat)(clip[1] + clip[3]) / (GLfloat)texture->height;\
            batch->vertex_buffer[index + 14] = (GLfloat)(clip[0] + clip[2]) / (GLfloat)texture->width;\
            batch->vertex_buffer[index + 15] = (GLfloat)(clip[1] + clip[3]) / (GLfloat)texture->height;\
        }\
            break;\
        case SPRITE_FLIP_HORIZONTAL:\
        {\
            batch->vertex_buffer[index +  2] = (GLfloat)(clip[0] + clip[2]) / (GLfloat)texture->width;\
            batch->vertex_buffer[index +  3] = (GLfloat)clip[1] / (GLfloat)texture->height;\
            batch->vertex_buffer[index +  6] = (GLfloat)clip[0] / (GLfloat)texture->width;\
            batch->vertex_buffer[index +  7] = (GLfloat)clip[1] / (GLfloat)texture->height;\
            batch->vertex_buffer[index + 10] = (GLfloat)(clip[0] + clip[2]) / (GLfloat)texture->width;\
            batch->vertex_buffer[index + 11] = (GLfloat)(clip[1] + clip[3]) / (GLfloat)texture->height;\
            batch->vertex_buffer[index + 14] = (GLfloat)clip[0] / (GLfloat)texture->width;\
            batch->vertex_buffer[index + 15] = (GLfloat)(clip[1] + clip[3]) / (GLfloat)texture->height;\
        }\
            break;\
        case SPRITE_FLIP_VERTICAL:\
        {\
            batch->vertex_buffer[index +  2] = (GLfloat)clip[0] / (GLfloat)texture->width;\
            batch->vertex_buffer[index +  3] = (GLfloat)(clip[1] + clip[3]) / (GLfloat)texture->height;\
            batch->vertex_buffer[index +  6] = (GLfloat)(clip[0] + clip[2]) / (GLfloat)texture->width;\
            batch->vertex_buffer[index +  7] = (GLfloat)(clip[1] + clip[3]) / (GLfloat)texture->height;\
            batch->vertex_buffer[index + 10] = (GLfloat)clip[0] / (GLfloat)texture->width;\
            batch->vertex_buffer[index + 11] = (GLfloat)clip[1] / (GLfloat)texture->height;\
            batch->vertex_buffer[index + 14] = (GLfloat)(clip[0] + clip[2]) / (GLfloat)texture->width;\
            batch->vertex_buffer[index + 15] = (GLfloat)clip[1] / (GLfloat)texture->height;\
        }\
            break;\
        case SPRITE_FLIP_BOTH:\
        {\
            batch->vertex_buffer[index +  2] = (GLfloat)(clip[0] + clip[2]) / (GLfloat)texture->width;\
            batch->vertex_buffer[index +  3] = (GLfloat)(clip[1] + clip[3]) / (GLfloat)texture->height;\
            batch->vertex_buffer[index +  6] = (GLfloat)clip[0] / (GLfloat)texture->width;\
            batch->vertex_buffer[index +  7] = (GLfloat)(clip[1] + clip[3]) / (GLfloat)texture->height;\
            batch->vertex_buffer[index + 10] = (GLfloat)(clip[0] + clip[2]) / (GLfloat)texture->width;\
            batch->vertex_buffer[index + 11] = (GLfloat)clip[1] / (GLfloat)texture->height;\
            batch->vertex_buffer[index + 14] = (GLfloat)clip[0] / (GLfloat)texture->width;\
            batch->vertex_buffer[index + 15] = (GLfloat)clip[1] / (GLfloat)texture->height;\
        }\
            break;\
    }

#define SET_UNCLIPPED_SPRITE_FLIPPED_CLIPS()\
        switch (flip)\
        {\
            case SPRITE_FLIP_NONE:\
            {\
                batch->vertex_buffer[index +  2] = 0.0f;\
                batch->vertex_buffer[index +  3] = 0.0f;\
                batch->vertex_buffer[index +  6] = 1.0f;\
                batch->vertex_buffer[index +  7] = 0.0f;\
                batch->vertex_buffer[index + 10] = 0.0f;\
                batch->vertex_buffer[index + 11] = 1.0f;\
                batch->vertex_buffer[index + 14] = 1.0f;\
                batch->vertex_buffer[index + 15] = 1.0f;\
            }\
                break;\
            case SPRITE_FLIP_HORIZONTAL:\
            {\
                batch->vertex_buffer[index +  2] = 1.0f;\
                batch->vertex_buffer[index +  3] = 0.0f;\
                batch->vertex_buffer[index +  6] = 0.0f;\
                batch->vertex_buffer[index +  7] = 0.0f;\
                batch->vertex_buffer[index + 10] = 1.0f;\
                batch->vertex_buffer[index + 11] = 1.0f;\
                batch->vertex_buffer[index + 14] = 0.0f;\
                batch->vertex_buffer[index + 15] = 1.0f;\
            }\
                break;\
            case SPRITE_FLIP_VERTICAL:\
            {\
                batch->vertex_buffer[index +  2] = 0.0f;\
                batch->vertex_buffer[index +  3] = 1.0f;\
                batch->vertex_buffer[index +  6] = 1.0f;\
                batch->vertex_buffer[index +  7] = 1.0f;\
                batch->vertex_buffer[index + 10] = 0.0f;\
                batch->vertex_buffer[index + 11] = 0.0f;\
                batch->vertex_buffer[index + 14] = 1.0f;\
                batch->vertex_buffer[index + 15] = 0.0f;\
            }\
                break;\
            case SPRITE_FLIP_BOTH:\
            {\
                batch->vertex_buffer[index +  2] = 1.0f;\
                batch->vertex_buffer[index +  3] = 1.0f;\
                batch->vertex_buffer[index +  6] = 0.0f;\
                batch->vertex_buffer[index +  7] = 1.0f;\
                batch->vertex_buffer[index + 10] = 1.0f;\
                batch->vertex_buffer[index + 11] = 0.0f;\
                batch->vertex_buffer[index + 14] = 0.0f;\
                batch->vertex_buffer[index + 15] = 0.0f;\
            }\
                break;\
        }

#define ROTATE_SPRITE()\
    float trans_x, trans_y, rot_x, rot_y;\
    \
    for (int i = 0; i < 4; ++i)\
    {\
        trans_x = batch->vertex_buffer[index + (i * 4) + 0] - origin_x;\
        trans_y = batch->vertex_buffer[index + (i * 4) + 1] - origin_y;\
        \
        rot_x = trans_x * cos(angle) - trans_y * sin(angle);\
        rot_y = trans_x * sin(angle) + trans_y * cos(angle);\
        \
        batch->vertex_buffer[index + (i * 4) + 0] = rot_x + origin_x;\
        batch->vertex_buffer[index + (i * 4) + 1] = rot_y + origin_y;\
    }

int
SpriteBatch_init(SpriteBatch *batch, GLchar *error_log, GLint log_len)
{
    batch->tex_swap_count = 0;
    batch->sprite_count   = 0;
    batch->scale_x = 1.0f;
    batch->scale_y = 1.0f;

    batch->vertex_buffer    = (GLfloat*)malloc(BATCH_SIZE * FLOATS_PER_SPRITE * sizeof(GLfloat));
    batch->texture_buffer   = (TextureBufferItem*)malloc(BATCH_SIZE * sizeof(TextureBufferItem));
    batch->buffer_size = BATCH_SIZE;

    const char *vertex_code =
        "#version 100\n"
        "attribute vec2 pos;\n"
        "attribute vec2 in_tex_coord;\n"
        "varying vec2 tex_coord;\n"
        "uniform mat4 p;\n"
        "void main(void)\n"
        "{\n"
        "   gl_Position = p * vec4(pos.x, pos.y, 0.0f, 1.0f);\n"
        "   tex_coord = in_tex_coord;\n"
        "}";

    const char *fragment_code =
        "#version 100\n"
        "varying vec2 tex_coord;\n"
        "uniform sampler2D tex;\n"
        "void main(void)\n"
        "{\n"
        "   gl_FragColor = texture2D(tex, tex_coord);\n"
        "}";

    GLuint vtx_shader = glCreateShader(GL_VERTEX_SHADER);
    GLuint frg_shader = glCreateShader(GL_FRAGMENT_SHADER);

    glShaderSource(vtx_shader, 1, &vertex_code, 0);
    glShaderSource(frg_shader, 1, &fragment_code, 0);
    glCompileShader(vtx_shader);
    glCompileShader(frg_shader);

    GLint success;

    glGetShaderiv(vtx_shader, GL_COMPILE_STATUS, &success);
    if (!success)
    {
        if (error_log)
            glGetShaderInfoLog(vtx_shader, log_len, NULL, error_log);
        glDeleteShader(vtx_shader);
        glDeleteShader(frg_shader);
        return 2;
    }

    glGetShaderiv(frg_shader, GL_COMPILE_STATUS, &success);
    if (!success)
    {
        if (error_log)
            glGetShaderInfoLog(frg_shader, log_len, NULL, error_log);
        glDeleteShader(vtx_shader);
        glDeleteShader(frg_shader);
        return 3;
    }

    GLuint program = glCreateProgram();
    glAttachShader(program, vtx_shader);
    glAttachShader(program, frg_shader);
    glLinkProgram(program);
    glDeleteShader(vtx_shader);
    glDeleteShader(frg_shader);

    glGetProgramiv(program, GL_LINK_STATUS, &success);
    if (!success)
    {
        if (error_log)
            glGetProgramInfoLog(program, log_len, NULL, error_log);
        glDeleteProgram(program);
        return 4;
    }

    batch->program = program;

    GLint pos_loc      = glGetAttribLocation(batch->program, "pos");
    GLint tex_crd_loc  = glGetAttribLocation(batch->program, "in_tex_coord");
    batch->proj_loc    = glGetUniformLocation(batch->program, "p");

    if (pos_loc < 0)
        return 5;

    if (tex_crd_loc < 0)
        return 6;

    if (batch->proj_loc < 0)
        return 7;

    glGenBuffers(1, &batch->vbo);
    glBindBuffer(GL_ARRAY_BUFFER, batch->vbo);
    glBufferData(GL_ARRAY_BUFFER,
        batch->buffer_size * FLOATS_PER_SPRITE * sizeof(GLfloat),
        batch->vertex_buffer, GL_DYNAMIC_DRAW);

    glGenBuffers(1, &batch->ebo);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, batch->ebo);
    GLuint indices[6] = {0, 1, 2, 2, 3, 1};
    GLuint *indice_data = (GLuint*)malloc(BATCH_SIZE * 6 * sizeof(GLuint));
    for (int i = 0; i < BATCH_SIZE; ++i)
        for (int j = 0; j < 6; ++j)
            indice_data[i * 6 + j] = indices[j] + i * 4;
    glBufferData(GL_ELEMENT_ARRAY_BUFFER,
                 BATCH_SIZE * 6 * sizeof(GLuint),
                 indice_data,
                 GL_DYNAMIC_DRAW);
    free(indice_data);

    glVertexAttribPointer(pos_loc, 2, GL_FLOAT, GL_FALSE,
        4 * sizeof(GLfloat), (const GLvoid*)0);
    glEnableVertexAttribArray(pos_loc);

    glVertexAttribPointer(tex_crd_loc, 2, GL_FLOAT, GL_FALSE,
        4 * sizeof(GLfloat), (const GLvoid*)(2 * sizeof(GLfloat)));
    glEnableVertexAttribArray(tex_crd_loc);

    return 0;
};

void
SpriteBatch_allocateMore(SpriteBatch *batch)
{
    unsigned int new_size = (unsigned int)((float)batch->buffer_size * 1.05f);
    if (new_size <= batch->buffer_size)
        new_size = batch->buffer_size + 1;

    void *vertex_buf  = malloc(new_size * FLOATS_PER_SPRITE* sizeof(GLfloat));
    void *tex_buf     = malloc(new_size * FLOATS_PER_SPRITE * sizeof(TextureBufferItem));

    memcpy(vertex_buf, batch->vertex_buffer,
        batch->buffer_size * FLOATS_PER_SPRITE * sizeof(GLfloat));
    memcpy(tex_buf, batch->texture_buffer,
        batch->buffer_size * sizeof(TextureBufferItem));

    free(batch->vertex_buffer);
    free(batch->texture_buffer);

    batch->vertex_buffer    = (GLfloat*)vertex_buf;
    batch->texture_buffer   = (TextureBufferItem*)tex_buf;

    batch->buffer_size = new_size;
}

void
SpriteBatch_dispose(SpriteBatch *batch)
{
    free(batch->vertex_buffer);
    free(batch->texture_buffer);
    glDeleteBuffers(1, &batch->vbo);
    glDeleteProgram(batch->program);
}

void
SpriteBatch_flush(SpriteBatch *batch, int *vp)
{
    if (batch->sprite_count < 1)
        return;

    glUseProgram(batch->program);

    /* Set up the projection matrix by screen dimensions */
    GLint viewport[4];

    if (vp)
    {
        viewport[0] = vp[0];
        viewport[1] = vp[1];
        viewport[2] = vp[2];
        viewport[3] = vp[3];
    }
    else
    {
        glGetIntegerv(GL_VIEWPORT, viewport);
    }

    GLfloat projection[4][4] =
    {
        {2.0f / (GLfloat)(viewport[2] - viewport[0]), 0.0f, 0.0f, 0.0f},
        {0.0f, 2.0f / (GLfloat)(viewport[1] - viewport[3]), 0.0f, 0.0f},
        {0.0f, 0.0f, -1.0f, 0.0f},
        {- (GLfloat)(viewport[2] + viewport[0]) / (GLfloat)(viewport[2] - viewport[0]),
         - (GLfloat)(viewport[1] + viewport[3]) / (GLfloat)(viewport[1] - viewport[3]),
         0.0f, 1.0f}
    };

    glUniformMatrix4fv(batch->proj_loc, 1, GL_FALSE, &projection[0][0]);

    /* Buffer data */
    glBindBuffer(GL_ARRAY_BUFFER, batch->vbo);
    glActiveTexture(GL_TEXTURE0);

    unsigned int num_batches = batch->sprite_count / BATCH_SIZE + 1;
    unsigned int sprites_in_batch;
    unsigned int batch_sprites_drawn;
    unsigned int tex_buf_offset = 0;
    unsigned int repeats;
    GLuint last_texture = batch->texture_buffer[0].id;
    GLuint cur_texture;
    glBindTexture(GL_TEXTURE_2D, last_texture);

    for (unsigned int batch_num = 0; batch_num < num_batches; ++batch_num)
    {
        sprites_in_batch = batch->sprite_count - batch_num * BATCH_SIZE;
        if (sprites_in_batch > BATCH_SIZE) sprites_in_batch = BATCH_SIZE;

        batch_sprites_drawn = 0;

        glBufferSubData(GL_ARRAY_BUFFER,                                                    /* Target */
                        0,                                                                  /* Offset */
                        sprites_in_batch * FLOATS_PER_SPRITE * sizeof(GLfloat),             /* Size */
                        &batch->vertex_buffer[batch_num * BATCH_SIZE * FLOATS_PER_SPRITE]); /* Data */

        while (batch_sprites_drawn < sprites_in_batch)
        {
            /* Bind the correct texture */
            cur_texture = batch->texture_buffer[tex_buf_offset].id;

            if (cur_texture != last_texture)
            {
                glBindTexture(GL_TEXTURE_2D, batch->texture_buffer[tex_buf_offset].id);
                last_texture = cur_texture;
            }

            /* Draw the amount of repeats suggested by the texture buffer item */
            repeats = batch->texture_buffer[tex_buf_offset].repeats + 1;

            if (batch_sprites_drawn + repeats > sprites_in_batch)
            {
                repeats = sprites_in_batch - batch_sprites_drawn;
                batch->texture_buffer[tex_buf_offset].repeats -= repeats;
            }
            else
            {
                ++tex_buf_offset;
            }

            glDrawElements(GL_TRIANGLES, 	/* mode */
                           repeats * 6,		/* count */
                           GL_UNSIGNED_INT,	/* type */
                           (GLvoid*)(batch_sprites_drawn * 6 * sizeof(GLuint))); /* indices*/
            batch_sprites_drawn += repeats;
        }
    }

    batch->tex_swap_count = 0;
    batch->sprite_count   = 0;
}

void
SpriteBatch_drawSprite(SpriteBatch *batch, Texture *texture,
                       int x, int y, unsigned int *clip)
{
    if (batch->sprite_count + 1 > batch->buffer_size)
        SpriteBatch_allocateMore(batch);

    unsigned int index = batch->sprite_count * FLOATS_PER_SPRITE;

    if (clip)
    {
        SET_CLIPPED_SPRITE_DEFAULT_POSITION();
        SET_CLIPPED_SPRITE_DEFAULT_CLIPS();
    }
    else
    {
        SET_UNCLIPPED_SPRITE_DEFAULT_POSITION();
        SET_UNCLIPPED_SPRITE_DEFAULT_CLIPS();
    }

    SPRITE_DRAW_FUNC_LOWER_HALF();
}

void
SpriteBatch_drawSprite_Flip(SpriteBatch *batch, Texture *texture,
    int x, int y, unsigned int *clip, SpriteFlip flip)
{
    if (batch->sprite_count + 1 > batch->buffer_size)
        SpriteBatch_allocateMore(batch);

    unsigned int index = batch->sprite_count * FLOATS_PER_SPRITE;

    if (clip)
    {
        SET_CLIPPED_SPRITE_DEFAULT_POSITION();
        SET_CLIPPED_SPRITE_FLIPPED_CLIPS();
    }
    else
    {
        SET_UNCLIPPED_SPRITE_DEFAULT_POSITION();
        SET_UNCLIPPED_SPRITE_FLIPPED_CLIPS();
    }

    SPRITE_DRAW_FUNC_LOWER_HALF();
}

void
SpriteBatch_drawSprite_Scale(SpriteBatch *batch, Texture *texture,
    int x, int y, unsigned int *clip, float scale_x, float scale_y)
{
    if (batch->sprite_count + 1 > batch->buffer_size)
        SpriteBatch_allocateMore(batch);

    unsigned int index = batch->sprite_count * FLOATS_PER_SPRITE;

    if (clip)
    {
        SET_CLIPPED_SPRITE_SCALED_POSITION();
        SET_CLIPPED_SPRITE_DEFAULT_CLIPS();
    }
    else
    {
        SET_UNCLIPPED_SPRITE_SCALED_POSITION();
        SET_UNCLIPPED_SPRITE_DEFAULT_CLIPS();
    }

    SPRITE_DRAW_FUNC_LOWER_HALF();
}

void
SpriteBatch_drawSprite_Rotate(SpriteBatch *batch, Texture *texture,
    int x, int y, unsigned int *clip, float angle)
{

    if (batch->sprite_count + 1 > batch->buffer_size)
        SpriteBatch_allocateMore(batch);

    unsigned int index = batch->sprite_count * FLOATS_PER_SPRITE;

    GLfloat origin_x;
    GLfloat origin_y;

    if (clip)
    {
        SET_CLIPPED_SPRITE_DEFAULT_POSITION();
        SET_CLIPPED_SPRITE_DEFAULT_CLIPS();
        origin_x = batch->scale_x * ((GLfloat)x + (GLfloat)clip[2]/2.0f);
        origin_y = batch->scale_y * ((GLfloat)y + (GLfloat)clip[3]/2.0f);
    }
    else
    {
        SET_UNCLIPPED_SPRITE_DEFAULT_POSITION();
        SET_UNCLIPPED_SPRITE_DEFAULT_CLIPS();
        origin_x = batch->scale_x * ((GLfloat)x + (GLfloat)texture->width/2.0f);
        origin_y = batch->scale_y * ((GLfloat)y + (GLfloat)texture->height/2.0f);
    }

    ROTATE_SPRITE();

    SPRITE_DRAW_FUNC_LOWER_HALF();
}

void
SpriteBatch_drawSprite_FlipScale(SpriteBatch *batch, Texture *texture,
    int x, int y, unsigned int *clip,SpriteFlip flip, float scale_x, float scale_y)
{
    if (batch->sprite_count + 1 > batch->buffer_size)
        SpriteBatch_allocateMore(batch);

    unsigned int index = batch->sprite_count * FLOATS_PER_SPRITE;

    if (clip)
    {
        SET_CLIPPED_SPRITE_SCALED_POSITION();
        SET_CLIPPED_SPRITE_FLIPPED_CLIPS();
    }
    else
    {
        SET_UNCLIPPED_SPRITE_SCALED_POSITION();
        SET_UNCLIPPED_SPRITE_FLIPPED_CLIPS();
    }

    SPRITE_DRAW_FUNC_LOWER_HALF();
}

void
SpriteBatch_drawSprite_FlipRotate(SpriteBatch *batch, Texture *texture,
    int x, int y, unsigned int *clip, SpriteFlip flip, float angle)
{
    if (batch->sprite_count + 1 > batch->buffer_size)
        SpriteBatch_allocateMore(batch);

    unsigned int index = batch->sprite_count * FLOATS_PER_SPRITE;

    GLfloat origin_x;
    GLfloat origin_y;

    if (clip)
    {
        SET_CLIPPED_SPRITE_DEFAULT_POSITION();
        SET_CLIPPED_SPRITE_FLIPPED_CLIPS();
        origin_x = batch->scale_x * ((GLfloat)x + (GLfloat)clip[2]/2.0f);
        origin_y = batch->scale_y * ((GLfloat)y + (GLfloat)clip[3]/2.0f);
    }
    else
    {
        SET_UNCLIPPED_SPRITE_DEFAULT_POSITION();
        SET_UNCLIPPED_SPRITE_FLIPPED_CLIPS();
        origin_x = batch->scale_x * ((GLfloat)x + (GLfloat)texture->width/2.0f);
        origin_y = batch->scale_y * ((GLfloat)y + (GLfloat)texture->height/2.0f);
    }

    ROTATE_SPRITE();
    SPRITE_DRAW_FUNC_LOWER_HALF();
}

void
SpriteBatch_drawSprite_ScaleRotate(SpriteBatch *batch, Texture *texture,
    int x, int y, unsigned int *clip, float scale_x, float scale_y, float angle)
{
    if (batch->sprite_count + 1 > batch->buffer_size)
        SpriteBatch_allocateMore(batch);

    unsigned int index = batch->sprite_count * FLOATS_PER_SPRITE;

    GLfloat origin_x;
    GLfloat origin_y;

    if (clip)
    {
        SET_CLIPPED_SPRITE_SCALED_POSITION();
        SET_CLIPPED_SPRITE_DEFAULT_CLIPS();
        origin_x = batch->scale_x * ((GLfloat)x + (GLfloat)clip[2]/2.0f);
        origin_y = batch->scale_y * ((GLfloat)y + (GLfloat)clip[3]/2.0f);
    }
    else
    {
        SET_UNCLIPPED_SPRITE_SCALED_POSITION();
        SET_UNCLIPPED_SPRITE_DEFAULT_CLIPS();
        origin_x = batch->scale_x * ((GLfloat)x + (GLfloat)texture->width/2.0f);
        origin_y = batch->scale_y * ((GLfloat)y + (GLfloat)texture->height/2.0f);
    }

    ROTATE_SPRITE();
    SPRITE_DRAW_FUNC_LOWER_HALF();
}

void
SpriteBatch_drawSprite_FlipScaleRotate(SpriteBatch *batch, Texture *texture,
    int x, int y, unsigned int *clip,
    SpriteFlip flip, float scale_x, float scale_y, float angle)
{
    if (batch->sprite_count + 1 > batch->buffer_size)
        SpriteBatch_allocateMore(batch);

    unsigned int index = batch->sprite_count * FLOATS_PER_SPRITE;

    GLfloat origin_x;
    GLfloat origin_y;

    if (clip)
    {
        SET_CLIPPED_SPRITE_SCALED_POSITION();
        SET_CLIPPED_SPRITE_FLIPPED_CLIPS();
        origin_x = batch->scale_x * ((GLfloat)x + (GLfloat)clip[2]/2.0f);
        origin_y = batch->scale_y * ((GLfloat)y + (GLfloat)clip[3]/2.0f);
    }
    else
    {
        SET_UNCLIPPED_SPRITE_SCALED_POSITION();
        SET_UNCLIPPED_SPRITE_FLIPPED_CLIPS();
        origin_x = batch->scale_x * ((GLfloat)x + (GLfloat)texture->width/2.0f);
        origin_y = batch->scale_y * ((GLfloat)y + (GLfloat)texture->height/2.0f);
    }

    ROTATE_SPRITE();
    SPRITE_DRAW_FUNC_LOWER_HALF();
}
